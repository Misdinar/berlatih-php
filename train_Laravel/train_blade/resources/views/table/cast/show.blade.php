@extends('master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Show Cast</h3>
      </div>
    <div class="card-body">
        <h4>{{$cast->nama}} <b>{{$cast->umur}}</b></h4>
        <p>{{$cast->bio}}</p>
    </div>
</div>
@endsection
