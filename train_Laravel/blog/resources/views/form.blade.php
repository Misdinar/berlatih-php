<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3> 
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label> <br><br>
        <input type="text" name="first-name"> <br><br>

        <label>last Name:</label> <br><br>
        <input type="text" name="last-name"> <br><br>

        <label>Gender</label> <br><br>
        <input type="radio" name="Gender">Male <br>
        <input type="radio" name="Gender">Female <br>
        <input type="radio" name="Gender">Other <br><br>

        <label>Nationality</label> <br><br>
        <select name="nationality">
            <option value="INA">Indonesian</option>
            <option value="MYS">Malaysian</option>
            <option value="SGP">Singaporean</option>
            <option value="AUS">Australian</option>
        </select> <br><br>

        <label>Language Spoken:</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> Chinese (Traditional) <br>
        <input type="checkbox"> Chinese (Simplified) <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Melayu <br>
        <input type="checkbox"> Other <br><br>
        <label>Bio:</label> <br><br>
        
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>