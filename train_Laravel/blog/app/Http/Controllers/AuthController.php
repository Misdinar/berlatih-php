<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function form_post(Request $request){
        $fnama = $request["first-name"];
        $lname = $request["last-name"];
        $fullname = $fnama . " " . $lname;
        return view('welcome', ['name'=>$fullname]);
    }
}
